/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author iHC
 */
public class Lab2 {
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char Player = 'x';
    static int row,col;
    public static void main(String[] args) {            
        printWelcome();        
        while (true) {                     
            printTable();
            printTurn();
            inputRowCol();
            if(isWin()){
                printTable();
                printWin();                
                reset();
                if(Play()){                    
                    break;
                }
            }if(isDraw()){
                printTable();
                printDraw();
                reset();
                if(Play()){
                    break;
                }
            }SwitchPlayer();            
        }
               
        
}
    private static void printWelcome() {
        System.out.println("Welcome to the OX game");
    }
    private static void printTable() {
         for(int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
             System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(Player+" Turn");    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {            
            row=kb.nextInt();
            col=kb.nextInt();
            if(table[row-1][col-1]=='-'){
                table[row-1][col-1] = Player;
                return;
            }
        }       
    }

    private static void SwitchPlayer() {
        if(Player=='x'){
            Player='o';
    }else{
        Player = 'x';
        }
}

    private static boolean isWin() {
        if(Checkrow()){
            return true;
        }if(Checkcol()){
            return true;
        }if(Checkrowcol()){
            return true;
        }if(Checkcolrow()){
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println("---- "+Player+" You Win ----");
    }

    private static boolean Checkrow() {
        for(int i = 0;i<3;i++){
            if(table[row-1][i]!=Player){
                    return false;
            }                
        }
        return true;
    }

    private static boolean Checkcol() {
        for(int i = 0;i<3;i++){
            if(table[i][col-1]!=Player){
                    return false;
            }                
        }
        return true;
    }

    private static boolean Checkrowcol() {
        for(int i = 0;i<3;i++){
            if(table[i][i]!=Player){
                    return false;
            }                
        }
        return true;
}

    private static boolean Checkcolrow() {
        for(int i = 0;i<3;i++){
            if(table[i][2-i]!=Player){
                    return false;
            }                
        }
        return true;    
    }

    private static void printDraw() {
        System.out.println("---- Draw ----");
}

    private static boolean isDraw() {
        for(int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                if(table[i][j]=='-'){
                    return false;
                }
            }                
        }
        return true;
    }

    private static boolean Play() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Play Again? Yes(1)/N(2) ");
        int n = kb.nextInt();
        if(n==1){
            return false;
        }if(n==2){
           return true;                                
        }return true;
    }                

    private static void reset() {
        for(int i = 0;i<3;i++){
            for(int j = 0;j<3;j++){
                table[i][j]='-';
             
            }
        }                
    }
}
